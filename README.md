# Machine Learning for Communication_TUM



## Overview
Repository consists of 12 notebook files, which are the coding assigments for this course


## Files

tut01 - Introduction to pytorch and equalizer


tut02 - Information measures (expectation,entropy), building a NN in pytorch, non linear equalizer


tut03 - NN Demapper For AWGN Channel


tut04 - Bitwise NN Demapper


tut05 - NN Equalizers for attenuated BPSK in AWGN


tut06 - Training Neural Networks


tut07 - One-Hot Message Representation


tut08 - PCA


tut09 - Spectral Shaping


tut10 - Deterministic channel Modelling


tut11 - Linear Filters


tut12 - Recursive Demapper (using RNNs)

